= Install the prerequisites

sudo apt-get install nasm qemu hexcurse hexedit hexer ghex kvm-pxe

= Build and Run

make clean
make run

= Commands for examining the compiled instructions

hexdump -dv helloboot
hexdump -v helloboot

= The bin file needs to be exactly 512 bytes in length. 

Translate hex to decimal.
echo '$((0x200)):'$((0x200))
