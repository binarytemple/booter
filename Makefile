all: booter.img

booter.bin : booter.asm
		nasm -f bin -o $@ $<  

booter.img : booter.bin
	qemu-img create booter.img 1440
	dd if=booter.bin of=booter.img conv=notrunc

run: booter.img
	qemu -fda booter.img

clean:
	rm -f booter.img
	rm -f booter.bin
